## Смоук
- [Элементы интерфейса](../smoke/ui-elements.md)
- [Авторизация](../smoke/auth.md)
- [Лента](../smoke/feed.md)
- [Пост](../smoke/story.md)
- [Комментарии](../smoke/comments.md)
- [Подписки](../smoke/subs.md)
- [Игнор-лист](../smoke/ignore-list.md)
## Регрессия
- [Ленты](../regression/feed.md)
- [Профиль](../regression/profile.md)
- [И.т.д.](...)
